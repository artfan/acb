$('.first__slider').slick({
	arrows: true,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  prevArrow: $('.first__prev'),
  nextArrow: $('.first__next'),
});
$('.slider__marks').slick({
	arrows: true,
  infinite: true,
  slidesToShow: 7,
  slidesToScroll: 1,
  prevArrow: $('.marks__prev'),
  nextArrow: $('.marks__next'),
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
  ]
});
$('.slider__brands').slick({
	arrows: true,
	infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  prevArrow: $('.brands__prev'),
  nextArrow: $('.brands__next'),
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
$('.slider__azian-marks').slick({
	arrows: true,
	infinite: true,
  slidesToShow: 7,
  slidesToScroll: 1,
  prevArrow: $('.azian-marks__prev'),
  nextArrow: $('.azian-marks__next'),
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
  ]
});
$('.capacity__slider').slick({
  arrows: true,
  infinite: true,
  slidesToShow: 7,
  slidesToScroll: 1,
  prevArrow: $('.capacity__prev'),
  nextArrow: $('.capacity__next'),
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
$('.size__slider').slick({
  arrows: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  prevArrow: $('.size__prev'),
  nextArrow: $('.size__next'),
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.catalog__full').click(function(){
  $('.catalog__full').toggleClass('catalog__full--open');
});
$('.exchange__checkbox').click(function(){
  $(this).parent().toggleClass('one-buy__dop-info--exchange');
});
$('.header-main__place').click(function(){
  $('.place-list').toggleClass('header-main__place--open');
});
$('.place__variant').click(function(){
  $('.place__variant').removeClass('place--active');
  $(this).addClass('place--active');
});

$('.one-buy__delete').click(function(){
  $(this).parent().detach();
});
$('.flter__clear').click(function(){
  $('.filter')[0].reset();
});

$('.polarity').click(function(){
  $(this).toggleClass('polarity--activ');
});

$('.btn-more').click(function(){
  let item = $(".item").html();
  for (var i = 0; i < 4; i++) {
     $(this).parent().before('<div class="item"><div class="item__pluses"><!-- Если есть скидка на товар, добавляется этот элемент --><span class="item--sale">скидка <span class="sale__procent">33%</span></span><span class="item--hit">хит продаж</span></div><span class="item--avail">в наличии</span><img src="img/acb2.png" alt="" class="item__prev"><a href="item.html" class="item__name">Русская звезда 60R 430A</a><p class="item__price"><span>2300</span> ₽</p><a href="item.html" class="item__buy">купить</a><div class="item__info"><p class="info__characteristic">Емкость <span class="characteristic__data">60 A·ч</span></p><p class="info__characteristic">Пусковой ток: <span class="characteristic__data">450 А</span></p><p class="info__characteristic">Полярность: <span class="characteristic__data">Обратная</span></p><p class="info__characteristic">Габариты: <span class="characteristic__data">242x175x190</span></p></div></div>');
  
    // $(".content__items").prependTo(item[0]);
  }
})